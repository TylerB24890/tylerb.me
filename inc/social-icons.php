<ul class="social">
  <li><a href="https://github.com/TylerB24890"><span data-hover="Github">Github</span><i class="fa fa-github"></i></a></li>
  <li><a href="https://www.linkedin.com/in/tylerb24890"><span data-hover="LinkedIn">LinkedIn</span><i class="fa fa-linkedin"></i></a></li>
  <li><a href="https://twitter.com/TyBailey23"><span data-hover="Twitter">Twitter</span><i class="fa fa-twitter"></i></a></li>
  <li><a href="mailto:tylerb.media@gmail.com"><span data-hover="Email">Email</span><i class="fa fa-envelope"></i></a></li>
</ul>
