<?php require('functions.php'); ?>

<!DOCTYPE HTML>
<html>
	<head>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-41168416-1"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', 'UA-41168416-1');
		</script>
		
		<title>Tyler Bailey | Full Stack Developer</title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta description="Full stack web and mobile app developer living in Denver, CO.">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="@TyBailey23">
		<meta name="twitter:title" content="TylerB | Full Stack Developer | 2018 Portfolio">
		<meta name="twitter:description" content="Full stack web and mobile app developer living in Denver, CO.">
		<meta name="twitter:creator" content="@TyBailey23">
		<meta name="twitter:image" content="<?php echo $site_url; ?>/img/tylerb-social-media.jpg">

		<!-- Open Graph data -->
		<meta property="og:title" content="TylerB | Full Stack Developer | 2018 Portfolio" />
		<meta property="og:type" content="profile" />
		<meta property="og:url" content="https://tylerb.me" />
		<meta property="og:image" content="<?php echo $site_url; ?>/img/tylerb-social-media.jpg" />
		<meta property="og:description" content="Full stack web and mobile app developer living in Denver, CO." />
		<meta property="og:site_name" content="TylerB" />

		<link rel="stylesheet" href="styles/aos.css" />
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" />
		<link rel="stylesheet" href="styles/<?php echo (DEV ? 'style' : 'style.min'); ?>.css" />

		<script src="js/vendor/modernizr.custom.js"></script>

	</head>

<body>
	<div id="overlay"></div>

	<header>

		<div class="navbar navbar-default navbar-fixed-top alt">
			<div class="container-fluid">
				<div class="navbar-collapse">
					<ul class="nav navbar-nav navbar-right">
						<li>
							<a href="javascript:void(0);" id="trigger-overlay" class="nav-link">
								<i class="fa fa-user-circle"></i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</header>

	<?php include_once('inc/contact.php'); ?>
