

  <script src="js/vendor/jquery-2.2.4.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <script src="js/vendor/aos.js"></script>
  <script src="js/vendor/tilt.js"></script>
  <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
  <script src="js/swipe.js"></script>
  <script src="js/vendor/particles.min.js"></script>
  <script src="js/vendor/classie.js"></script>
  <script src="js/overlay.js"></script>
  <script src="js/<?php echo (DEV ? 'tylerb' : 'tylerb.min'); ?>.js"></script>
</body>
</html>
