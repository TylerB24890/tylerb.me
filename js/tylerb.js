/**
* Blackbaud Raisers Edge NXT Scripts
*
* @author	Elexicon 2017
*/

// Main Parallax -- Banners
$.fn.parallax = function(xpos, speedFactor, outerHeight) {
    var $this = jQuery(this);
    var getHeight;
    var firstTop;
    var paddingTop = 0;
    var $window = jQuery(window);
    var windowHeight = $window.height();

    //get the starting position of each element to have parallax applied to it
    $this.each(function(){
        firstTop = $this.offset().top - 140;
    });

    if (outerHeight) {
        getHeight = function(jqo) {
            return jqo.outerHeight(true);
        };
    } else {
        getHeight = function(jqo) {
            return jqo.height();
        };
    }

    // setup defaults if arguments aren't specified
    if (arguments.length < 1 || xpos === null) xpos = "50%";
    if (arguments.length < 2 || speedFactor === null) speedFactor = 0.1;
    if (arguments.length < 3 || outerHeight === null) outerHeight = true;

    // function to be called whenever the window is scrolled or resized
    function update(){
        var pos = $window.scrollTop();

        $this.each(function(){
            var $element = jQuery(this);
            var top = $element.offset().top;
            var height = getHeight($element);

            // Check if totally above or totally below viewport
            if (top + height < pos || top > pos + windowHeight) {
                return;
            }

            $this.css('backgroundPosition', xpos + " " + Math.round((firstTop + pos) * speedFactor) + "px");
        });
    }

    $window.bind('scroll', update).resize(update);
    update();
};


// On page load
(function($) {
	'use strict';

	var $window = $(window);
	var ww = $window.width();
	var $body = $('body');

	// URL Variables
	var pathArray = window.location.href.split( '/' );
	var protocol = pathArray[0];
	var host = pathArray[2];
	var directory = pathArray[3];
	var domainArray = host.split(".");
	var baseDomain = "";
	var domainExt = "";
	if(domainArray.length > 2) {
		baseDomain = domainArray[1];
		domainExt = domainArray[2];
	} else {
		baseDomain = domainArray[0];
		domainExt = domainArray[1];
	}
	var baseURL = "." + baseDomain + "." + domainExt;
	var fullURL = protocol + '//' + host;

	// Is touch device?
	var isTouch = !!("undefined" != typeof document.documentElement.ontouchstart)

	/**
	 * Add 'target="_blank"' to all external links
	 * @return string target=_blank attribute
	 */
	addLinkTarget(baseURL);

	/**
	* Animate the Bootstrap hamburger to an X on click
	*/
	$('.navbar-toggle').on('click', function() {
		$(this).toggleClass('active');
	});

	/**
	* Inline scrolling
	* @param  event e capture the JS click event
	* @return false - prevents link redirection
	*/
	$('a.inline-link').click(function(e) {
		var location = $(this).attr('href');

		$('html, body').animate({
			scrollTop: $(location).offset().top
		}, 1200);

		return false;
	});

	AOS.init({
		disable: 'mobile'
	});

	$('#work-carousel').slick({
		slidesToShow: 2,
		slidesToScroll: 1,
		swipeToSlide: true,
		focusOnSelect: true,
		variableWidth: true,
		centerMode: true,
		arrows: false,
		nextArrow: '<img class="slick-next slick-arrow" src="img/next-arrow.svg">',
		prevArrow: '<img class="slick-prev slick-arrow" src="img/prev-arrow.svg">',
	});

	$('#work-carousel').on('swipedown', function(e) {
		$('#swipe-msg').addClass('active');

		setTimeout(function() {
			$('#swipe-msg').removeClass('active');
		}, 1700);
	});

	const tilt = $('.project .project-img > img, .project .project-content').tilt({
		maxTilt: 10,
    perspective: 1400,
    easing: "cubic-bezier(.03,.98,.52,.99)",
    speed: 1200,
    glare: false,
    maxGlare: 0.2,
    scale: 1.01
	});


	$('#work .project .project-img img').on('click', function(e) {
		var project = $(this).parents('.project').attr('id');

		if($('#' + project).hasClass('slick-current')) {
			$('#' + project + ' a.project-btn')[0].click();
		}
	});

	// Disable animations/transitions until the page has loaded.
	$body.addClass('is-loading');

	$window.on('load', function() {
		window.setTimeout(function() {
			$body.removeClass('is-loading');
		}, 100);
	});

	$window.on('scroll', function() {
		var scrollPos = $window.scrollTop();
		fadeElement('.scroll-fade', scrollPos, 400);

		if(scrollPos >= $window.height() - 50) {
			$('.navbar.navbar-default').removeClass('alt');
		} else {
			$('.navbar.navbar-default').addClass('alt');
		}
	});

	$('#work').mousemove(function(event) {
	  var workWidth = $(window).width();
	  var workHeight = $(window).innerHeight();

	  var mouseXpercentage = Math.round(event.pageX / (workWidth / 2) * 50);
	  var mouseYpercentage = Math.round(event.pageY / (workHeight / 2) * 15);

	  $('#work-carousel').css('background', 'radial-gradient(ellipse at ' + mouseXpercentage + '% ' + mouseYpercentage + '%, #333333 5%, #232526 50%)');
	});

})(jQuery);

function fadeElement(el, scrollPos, val) {
	//Scroll and fade out the banner text
	jQuery(el).css({
		'margin-top' : -(scrollPos/3)+"px",
		'opacity' : 1-(scrollPos/val)
	});
}

/**
 * Add 'target="_blank"' to all external links
 * @param	 string URL to match against
 * @return string target=_blank attribute
 */
function addLinkTarget(url) {
	$('a').each(function() {
		$(this).attr('target', (this.href.match( url )) ? '_self' :'_blank');
	});
}
