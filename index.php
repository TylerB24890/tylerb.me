<?php include_once('inc/header.php'); ?>

		<div id="content">
			<section id="top">
				<div id="particle-canvas"></div>
				<div class="container-fluid">
					<div class="scroll-fade">
						<h2>Tyler Bailey</h2>
						<h3>< Building a Better Internet ></h3>
					</div>

					<a href="#work" class="more inline-link">2018 Portfolio</a>
				</div>
			</section>

			<section id="work">

				<div id="work-carousel">
					<div id="health-beat" class="project">
						<div class="container-fluid">
							<div class="col-md-6 col-md-push-6">
								<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
									<h2>Spectrum Health<span>Health Beat</span></h2>
									<p>
										<a href="https://spectrumhealth.org">Spectrum Health's</a> award winning brand journalism website contains over 2,600+ published articles by over 50 different authors &amp; guests. Great content, beautiful photography and a smooth user experience have earned this health blog 13 international awards.
									</p>

									<a class="project-btn" href="https://healthbeat.spectrumhealth.org">View project</a>
								</div>
							</div>
							<div class="col-md-6 col-md-pull-6 no-left-padding">
								<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
									<img src="img/healthbeat1.png">
								</div>
							</div>
						</div>
					</div>

					<div id="progressive-ae" class="project">
						<div class="col-md-6 col-md-push-6">
							<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
								<h2>Progressive AE</h2>
								<p>
									An architecture and design firm out of Grand Rapids, Michigan. Progressive AE needed a new website that highlighted their broad portfolio in a user-friendly way. Through a custom portfolio filtering solution, percise categorization and high-quality imagery, discover who Progressive AE is and the beautiful work they do around the country.
								</p>

								<a class="project-btn" href="http://www.progressiveae.com/">View project</a>
							</div>
						</div>
						<div class="col-md-6 no-left-padding col-md-pull-6">
							<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
								<img src="img/pae1.png">
							</div>
						</div>
					</div>

					<div id="terryberry" class="project">
						<div class="container-fluid">
							<div class="col-md-6 col-md-push-6">
								<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
									<h2>Terryberry</h2>
									<p>
										An industry leader in employee recognition, Terryberry provides employee recognition software to over 25,000 clients world wide. Browse through their extensive list of products and check out some of the useful resources available on the website.
									</p>

									<a class="project-btn" href="https://terryberry.com/">View project</a>
								</div>
							</div>
							<div class="col-md-6 no-left-padding col-md-pull-6">
								<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
									<img src="img/terryberry1.png">
								</div>
							</div>
						</div>
					</div>

					<div id="tedx" class="project">
						<div class="container-fluid">
							<div class="col-md-6 col-md-push-6">
								<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
									<h2>TEDx<span>Charleston, S.C.</span></h2>
									<p>
										<a href="https://www.ted.com/">TED</a> is a nonprofit organization devoted to "Ideas Worth Spreading." Browse an archive of fantastic TED talk videos, learn about upcoming speakers and events, and be inspired by the unique minds of Charleston, South Carolina.
									</p>

									<a class="project-btn" href="https://tedxcharleston.org">View project</a>
								</div>
							</div>
							<div class="col-md-6 no-left-padding col-md-pull-6">
								<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
									<img src="img/tedx1.png">
								</div>
							</div>
						</div>
					</div>

					<div id="blackbaud" class="project">
						<div class="container-fluid">
							<div class="col-md-6 col-md-push-6">
								<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
									<h2>Blackbaud</h2>
									<p>
							 		<a href="https://blackbaud.com">Blackbaud</a> is the world's leading cloud software company powering social good. Below you will find <i>just a few</i> of Blackbaud's properties I have had the pleasure of working with them on.
									</p>
									<a class="project-btn stacked" href="https://npengage.com">npEngage</a>
									<div class="clear clearfix"></div>
									<a class="project-btn stacked" href="https://institute.blackbaud.com/">Blackbaud Institute</a>
									<div class="clear clearfix"></div>
									<a class="project-btn stacked" href="https://showcase.blackbaud.com/">Blackbaud Showcase</a>
									<div class="clear clearfix"></div>
									<a class="project-btn stacked" href="https://hi.blackbaud.com/healthcareanalytics/">Blackbaud H.C. Analytics</a>
								</div>
							</div>
							<div class="col-md-6 no-left-padding col-md-pull-6">
								<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
									<img src="img/blackbaud1.png">
								</div>
							</div>
						</div>
					</div>

					<div id="budget" class="project">
						<div class="container-fluid">
							<div class="col-md-6 col-md-push-6">
								<div class="project-content" data-aos="fade-left" data-aos-duration="1700">
									<h2>Budget Sumthing</h2>
									<p>
										A cross-platform D.I.Y. budgeting application built on Facebook's <a href="https://facebook.github.io/react-native/">React Native</a> Javascript framework. Enter a starting balance and budget on the go.
										<span style="font-size: 12px; display: block; margin-top: 15px;">Coming soon to Android.</span>
									</p>

									<a class="project-btn" href="https://itunes.apple.com/us/app/budget-sumthing/id1305150019"></a>
								</div>
							</div>
							<div class="col-md-6 no-left-padding col-md-pull-6">
								<div class="project-img" data-aos="fade-right" data-aos-duration="1700">
									<img src="img/budget1.png">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div id="swipe-msg" class="hidden-lg hidden-md">
					<img src="//icongr.am/feather/chevron-left.svg?size=19&color=FFFFFF" />
					Swipe
					<img src="//icongr.am/feather/chevron-right.svg?size=19&color=FFFFFF" />
				</div>

				<div class="clear clearfix"></div>
			</section>

			<!--
			<section id="about">
				<div class="container-fluid">
					<h2></h2>
				</div>
			</section>
		-->
	<?php include_once('inc/footer.php'); ?>
		</div>
